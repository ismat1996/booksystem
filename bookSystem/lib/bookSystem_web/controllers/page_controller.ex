defmodule BookSystemWeb.PageController do
  use BookSystemWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
