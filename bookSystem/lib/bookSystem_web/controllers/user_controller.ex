defmodule BookSystemWeb.UserController do
    use BookSystemWeb, :controller
    alias BookSystem.Book.Loans
    alias BookSystem.Repo
    import Ecto.Query, only: [from: 2]
  
    def loans(conn, _params) do
        query = from l in Loans,
                select: {l.title, l.code, l.start_date, l.end_date, l.times_extended}
        conn
        |> put_flash(:info, "Your loans")
        |> render "loans.html", tuples: Repo.all(query)
    end

    def extend(conn, _params) do
        query = from l in Loans,
                select: {l.title, l.code, l.start_date, l.end_date, l.times_extended}
        conn
        |> put_flash(:info, "extended")
        |> render "loans.html", tuples: Repo.all(query)
    end
  end
  