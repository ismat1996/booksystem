defmodule BookSystemWeb.Router do
  use BookSystemWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", BookSystemWeb do
    pipe_through :browser # Use the default browser stack
    get "/", PageController, :index
    get "/user/loans", UserController, :loans
    get "/user/extendLoad", UserController, :extend
  end
end
