defmodule BookSystem.Repo.Migrations.CreateLoan do
  use Ecto.Migration

  def change do
    create table(:loans) do
      add :title, references(:catalogs, column: :title, type: :string, on_delete: :nothing)
      add :code, :string
      add :start_date, :string
      add :end_date, :string
      add :times_extended, :string

      timestamps()
    end

  end
end