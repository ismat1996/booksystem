defmodule BookSystem.Repo.Migrations.CreateCatalog do
  use Ecto.Migration

  def change do
    create table(:catalogs) do
      add :title, :string
      add :code, :string

      timestamps()
    end
    create unique_index(:catalogs, [:title], name: :title_uk)
  end
end