alias BookSystem.Book.Loans
alias BookSystem.Book.Catalogs
alias BookSystem.Repo


[%{code: "A", title: "F#"},
%{code: "B", title: "Elixir"}]
|> Enum.map(fn book -> Catalogs.changeset(%Catalogs{}, book) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)


[%{title: "F#",code: "A", start_date: "09-01-2019", end_date: "16-01-2018", times_extended: "0"},
%{title: "Elixir", code: "A", start_date: "19-12-2018",  end_date: "16-01-2019", times_extended: "4"}]
|> Enum.map(fn loan -> Loans.changeset(%Loans{}, loan) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)