# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :bookSystem,
  ecto_repos: [BookSystem.Repo]

# Configures the endpoint
config :bookSystem, BookSystemWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "+c7dsdqDqaOYhzCFAjS3bWa5F4pA/7UGETLmGuW9wyEQDFFQbbKZ1a62lfenj9Op",
  render_errors: [view: BookSystemWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: BookSystem.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
