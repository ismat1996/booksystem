Feature: extending feature
  As a user I go to destination
  I waant to extend my selected loan

  Scenario: Booking via STRS' web page (with confirmation)
    Given the following books I have taken
          | title  | code	 | start_date    | end_date   | times_extended|
          | F#     | A        | 09-01-2019	 | 16-01-2018 | 0             |
          | Elixir | A       | 19-12-2018	 | 16-01-2019 | 4             |       
    And I want to extend "F#" 
    And I open the web page
    And I click the link
    Then I should receive a confirmation message