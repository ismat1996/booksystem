defmodule WhiteBreadContext do
  use WhiteBread.Context
  alias BookSystem.Book.Loans
  use Hound.Helpers
  alias BookSystem.Repo

  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)    
    %{}
  end
  scenario_starting_state fn state ->
    Hound.start_session
    Ecto.Adapters.SQL.Sandbox.checkout(BookSystem.Repo)
    Ecto.Adapters.SQL.Sandbox.mode(BookSystem.Repo, {:shared, self()})
    %{}
  end
  
  given_ ~r/^the following books I have takent$/, fn state, %{table_data: table} ->
    table
    |> Enum.map(fn book -> Loans.changeset(%Loans{}, book) end)
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
    {:ok, state}
  end

  and_ ~r/^I want to extend "(?<book>[^"]+)"$/,
  fn state, %{book: book} ->
    {:ok, state |> Map.put(:book, book)}
  end
  and_ ~r/^I open the web page$/, fn state ->
    navigate_to "/user/loans"
    {:ok, state}
  end
  
  when_ ~r/^I click the Extend$/, fn state ->
    click({:id,"link"})
    redirect
    {:ok, state}
  end

  then_ ~r/^I should receive a confirmation message$/, fn state ->
    assert visible_in_page? ~r/extended/
    {:ok, state}  
  end

  scenario_finalize fn _status, _state ->
    Ecto.Adapters.SQL.Sandbox.checkin(BookSystem.Repo)
    # Hound.end_session
    nil
  end

end


